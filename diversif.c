/*
 * diversif - mainline.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/pagemap.h>
#include <linux/fs.h>
#include <linux/parser.h>
#include <asm/atomic.h>
#include <asm/uaccess.h>

MODULE_AUTHOR("Lucas Woods");
MODULE_LICENSE("GPL");

#define LFS_MAGIC 0x19980122
#define TMPSIZE 20

/* 
 * MAX_SOURCE is the maximum number of filesystems that can be defined at mount time
 * MAX_SOURCE_NAME is the maximum length of the filesystem mountpoint (this will be
 *  changed later to whatever the max pat is
 */

#define MAX_SOURCE 5
#define MAX_SOURCE_NAME 20

static int diversif_init(void) __init;
static void diversif_exit(void) __exit;

char sources[MAX_SOURCE][MAX_SOURCE_NAME];

enum { diversif_opt_sources,

};

static match_table_t tokens = {
	{diversif_opt_sources, "sources=%s"},
};

static int diversif_parse_options(char *options)
{

	char *p,*q,*src;
	int r = 0;
	substring_t args[MAX_OPT_ARGS];
	int token;
	while ((p = strsep(&options, ",")) != NULL) {
		if (!*p)
			continue;

		token = match_token(p, tokens, args);
		switch (token) {
		case diversif_opt_sources:
			{
				src = args[0].from;
				printk(KERN_INFO "diversif: Parsed options: \n");
				while((q = strsep(&src, ":")) != NULL) {
					if(!*q)
						continue;

					strcpy(&sources[r][0],q);
					printk(KERN_INFO "diversif:\t\t\t%s\n",q);
					r++;
				}
			}

		}

	}
	return 0;
}

static struct inode *diversif_make_inode(struct super_block *sb, int mode)
{
	struct inode *ret = new_inode(sb);

	if (ret) {
		ret->i_mode = mode;
		ret->i_uid = ret->i_gid = 0;
		ret->i_blocks = 0;
	}
	return ret;
}

static int diversif_open(struct inode *inode, struct file *filp)
{
	filp->private_data = inode->i_private;
	return 0;
}

static ssize_t diversif_read_file(struct file *filp, char *buf,
				  size_t count, loff_t * offset)
{

	printk(KERN_INFO "divesif: access to .diversif/sources! First fs is %s\n", &sources[0][0]);
	return 0;
	
}

static ssize_t diversif_write_file(struct file *filp, const char *buf,
				   size_t count, loff_t * offset)
{
	atomic_t *counter = (atomic_t *) filp->private_data;
	char tmp[TMPSIZE];
	if (*offset != 0)
		return -EINVAL;
	if (count >= TMPSIZE)
		return -EINVAL;
	memset(tmp, 0, TMPSIZE);
	if (copy_from_user(tmp, buf, count))
		return -EFAULT;
	atomic_set(counter, simple_strtol(tmp, NULL, 10));
	return count;
}

static struct file_operations diversif_file_ops = {
	.open = diversif_open,
	.read = diversif_read_file,
	.write = diversif_write_file,
};

static struct dentry *diversif_create_file(struct super_block *sb,
					   struct dentry *dir, const char *name
					   )
{
	struct dentry *dentry;
	struct inode *inode;
	struct qstr qname;
	qname.name = name;
	qname.len = strlen(name);
	qname.hash = full_name_hash(name, qname.len);
	dentry = d_alloc(dir, &qname);
	if (!dentry)
		goto out;
	inode = diversif_make_inode(sb, S_IFREG | 0644);
	if (!inode)
		goto out_dput;
	inode->i_fop = &diversif_file_ops;
	inode->i_private = sources;

	d_add(dentry, inode);
	return dentry;
      out_dput:
	dput(dentry);
      out:
	return 0;
}

static struct dentry *diversif_create_dir(struct super_block *sb,
					  struct dentry *parent,
					  const char *name)
{
	struct dentry *dentry;
	struct inode *inode;
	struct qstr qname;

	qname.name = name;
	qname.len = strlen(name);
	qname.hash = full_name_hash(name, qname.len);
	dentry = d_alloc(parent, &qname);
	if (!dentry)
		goto out;

	inode = diversif_make_inode(sb, S_IFDIR | 0644);
	if (!inode)
		goto out_dput;
	inode->i_op = &simple_dir_inode_operations;
	inode->i_fop = &simple_dir_operations;

	d_add(dentry, inode);
	return dentry;

      out_dput:
	dput(dentry);
      out:
	return 0;
}

static void diversif_create_files(struct super_block *sb, struct dentry *root)
{
	struct dentry *diversifdir;
	diversifdir = diversif_create_dir(sb, root, ".diversif");

	if (diversifdir)
		diversif_create_file(sb, diversifdir, "sources");
}

static struct super_operations diversif_s_ops = {
	.statfs = simple_statfs,
	.drop_inode = generic_delete_inode,
};

static int diversif_fill_super(struct super_block *sb, void *data, int silent)
{
	struct inode *root;
	struct dentry *root_dentry;
	sb->s_blocksize = PAGE_CACHE_SIZE;
	sb->s_blocksize_bits = PAGE_CACHE_SHIFT;
	sb->s_magic = LFS_MAGIC;
	sb->s_op = &diversif_s_ops;
	root = diversif_make_inode(sb, S_IFDIR | 0755);
	if (!root)
		goto out;
	root->i_op = &simple_dir_inode_operations;
	root->i_fop = &simple_dir_operations;
	root_dentry = d_alloc_root(root);
	if (!root_dentry)
		goto out_iput;
	sb->s_root = root_dentry;
	diversif_create_files(sb, root_dentry);
	return 0;

      out_iput:
	iput(root);
      out:
	return -ENOMEM;
}

static int diversif_get_super(struct file_system_type *fst,
					      int flags, const char *devname,
					      void *data, struct vfsmount *mnt)
{

	diversif_parse_options(data);
	return get_sb_single(fst, flags, data, diversif_fill_super, mnt);
}

static struct file_system_type diversif_type = {
	.owner = THIS_MODULE,
	.name = "diversif",
	.get_sb = diversif_get_super,
	.kill_sb = kill_litter_super,
};

static int __init diversif_init(void)
{
	return register_filesystem(&diversif_type);
}

static void __exit diversif_exit(void)
{
	unregister_filesystem(&diversif_type);
}

module_init(diversif_init);
module_exit(diversif_exit);
